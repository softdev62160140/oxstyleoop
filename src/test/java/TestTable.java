/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import bestza012.oxstyleobject.Table;
import bestza012.oxstyleobject.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author WIN10
 */
public class TestTable {
    
    public TestTable() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
    @Test
    public void testRow1ByX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.getWinner());
    }
    
    @Test
    public void testRow2ByX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.getWinner());
    }
    
    @Test
    public void testRow3ByX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.getWinner());
    }
    
    
    @Test
    public void testCol1ByX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.getWinner());
    }
    
     @Test
    public void testCol2ByX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.getWinner());
    }
    
    public void testCol3ByX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.getWinner());
    }
    
    public void testX1ByX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.getWinner());
    }
    
     public void testX2ByX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.getWinner());
    }
    
    
    @Test
    public void testRow1ByO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.getWinner());
    }
    
    @Test
    public void testRow2ByO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.getWinner());
    }
    
    @Test
    public void testRow3ByO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.getWinner());
    }
    
    
    @Test
    public void testCol1ByO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.getWinner());
    }
    
     @Test
    public void testCol2ByO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.getWinner());
    }
    
    public void testCol3ByO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.getWinner());
    }
    public void testX1ByO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.getWinner());
    }
    
     public void testX2ByO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.getWinner());
    }
     
     public void testIsFinishEqualsFalsebyX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.checkWin();
        assertEquals(false, table.isFinish());
    }
     public void testIsFinishEqualsFalse2byX(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.isFinish());
    }
     
    public void testIsFinishEqualsFalsebyO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
           table.switchPlayer();
        table.setRowCol(0, 0);
        table.checkWin();
        assertEquals(false, table.isFinish());
    }
     public void testIsFinishEqualsFalse2byO(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
           table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.isFinish());
    }
     
    public void checkDraw(){
        User x = new User('X');
        User o = new User('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 0);
        table.setRowCol(1, 2);
        table.setRowCol(2, 1);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(null,table.getWinner());
    }
}
